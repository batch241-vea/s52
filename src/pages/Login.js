import { useState, useEffect } from 'react';

import { Form, Button, h1 } from 'react-bootstrap';

export default function Login() {

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
   	const [isActive, setIsActive] = useState(false);

   	function loginUser(e){
   		e.preventDefault()

   		setEmail('');
        setPassword('');

        alert('You are now logged in!')
   	}

   	useEffect(()=> {
        if(email !== '' && password !== ''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password]);

	return (
		
        <Form onSubmit={(e) => loginUser(e)}>
        <h1>Login</h1><br/>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value = {email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password}
                    onChange = {e => setPassword(e.target.value)}
                    required
                />
            </Form.Group><br/>

            {isActive ?
            <Button variant="success" type="submit" id="submitBtn">
                Submit 
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn" disabled>
                Submit 
            </Button>
            }
        </Form>
    )

}